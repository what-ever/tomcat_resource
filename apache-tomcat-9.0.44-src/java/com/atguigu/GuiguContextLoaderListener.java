package com.atguigu;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class GuiguContextLoaderListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("容器创建了");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("容器销毁了");
    }
}
