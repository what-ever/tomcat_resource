# tomcat_resource

#### 介绍
该仓库用来存放本人在学习阅读tomcat源码时增加了一些注释之后的Tomcat源码，如果您选择将该仓库`git clone`
并且选择使用`idea`打开的话，那么之后可以直接debug点击绿色的debug运行即可，里面我直接添加了一些运行时的参数，但是请放心，里面并没有hack指令，您可以点击查看：

<img src="https://noteall.oss-cn-beijing.aliyuncs.com/tomcat/tomcat_gitee_repository/%E8%BF%90%E8%A1%8C%3A%E8%B0%83%E8%AF%95%E9%85%8D%E7%BD%AE.png" style="zoom:50%;" />

如果您在clone之后发现该目录下发现了`.idea`文件夹，建议您**`删除并且重新使用idea打开`**。避免不必要的麻烦且引入的一些jar容易下错位置而且不好排除及删除。

#### 软件架构

软件架构说明：
Tomcat软件架构参考雷锋阳老师的Tomcat源码课程中画的架构原理图

<img src="https://noteall.oss-cn-beijing.aliyuncs.com/tomcat/tomcat_gitee_repository/Tomcat%E6%9E%B6%E6%9E%84%E5%8E%9F%E7%90%86.jpg" style="zoom:50%;" />

如果看不清楚可以自己手动下载，链接：

[点击此处开始下载](https://noteall.oss-cn-beijing.aliyuncs.com/tomcat/tomcat_gitee_repository/Tomcat%E6%9E%B6%E6%9E%84%E5%8E%9F%E7%90%86.jpg)

#### 安装教程

当您打开项目的时候，您会发现里面的目录结构可能与在官网下载的的Tomcat源码有些不一样，因为我多加了一个`pom.xml`

将该项目变为了Maven项目。因为这样直接就能在本地启动，而不需要额外手动的下载一些运行时需要的依赖

> 当然这些工作也不是我做的，是雷老师做的

#### Tomcat启动时流程图

<img src="https://noteall.oss-cn-beijing.aliyuncs.com/tomcat/tomcat_gitee_repository/Tomcat%E7%9A%84%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B.jpg" style="zoom:50%;" />

当然这张图里面也包含了一些请求处理的时候的一些前置流程(在这张图的右下角是处理请求的时候的流程图)

当然如果看不清还是可以点击下方来进行下载：

[点我下载](https://noteall.oss-cn-beijing.aliyuncs.com/tomcat/tomcat_gitee_repository/Tomcat%E7%9A%84%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B.jpg)

最后：如果你不知道该从哪里入手的话，那么你可以看一下`org.apache.catalina.startup.Bootstrap`这个类，并且找到这个类里面的main方法，您就可以打开Tomcat世界的大门

> 当然如果您直接点击debug，也是可以的

如果**幸运**的话，当您clone下代码并且使用idea打开，当项目检索完成的时候，您将看见我打过的所有的断点，这样会使您事半功倍。

最后：如果您在学习Tomcat源码中遇到的任何问题，都可以与我进行探讨(如果是NIO的话就别找我了，我也不会)

祝大家在学习Tomcat的源码路上越走越通顺！
